<?php
$isValidFile = 0;
$isFileSelected = 0;
$isFormSubmitted = 0;
$subjectLine;
$headings = array();
$subheadings = array();
$subSubheadings = array();
$sender = 'hr@prdxn.com';
$note;

if(isset($_POST['doSendMail'])) {
  $isFormSubmitted = 1;
  $fileArrayData = $_FILES['uploadedFile']; //the infromation about the uploaded file name, size etc  
  if(isset($_FILES['uploadedFile'])) {
    $isFileSelected = 1;
    if(($fileArrayData['type'] === 'text/csv') || ($fileArrayData['type'] === 'application/vnd.ms-excel')) {
      $isValidFile = 1;
      $file = fopen($fileArrayData["tmp_name"],"r");
      $outsideArrC = 0;
      $dataArray = array();
      $startOfArrPtr = array();
      $k = 0;
      while (!feof($file)) {
        $line = fgetcsv($file);
        // var_dump($line);
        if($line) {
          // var_dump($line);
          if($k == 0) {
            $subjectLine = $line[0];
          }
          else if($k == 1) {
            foreach ($line as $heading) {
              if($heading) {
                array_push($headings, $heading);
              }
            }
          }
          else if($k == 2) {
            foreach ($line as $subheading) {
              if($subheading) {
                array_push($subheadings, $subheading);
              }
            }
          }
          else {
            $headers = array();
            $e_name = $line[0];
            $e_email = $line[1];
            // $carry_over = $line[2];
            $total_eligible_sl = $line[2];
            $total_eligible_vl = $line[3];
            $total_leav_tak_sl = $line[4];
            $total_leav_tak_vl = $line[5];
            $remain_sl = $line[6];
            $remain_vl = $line[7];
            $earned_comp = $line[8];
            $taken_comp = $line[9];
            $remain_comp = $line[10];
            $net_leave = $line[11];
            $total_penalty = $line[11];
            $outPutStr  = '';
            $outPutStr .= '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
                           "http://www.w3.org/TR/html4/loose.dtd">

                        <html lang="en">
                        <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                            <meta name="viewport" content="width=device-width , initial-scale=1, maximum-scale=1 user-scalable=no">
                            <title>Leave Status</title>
                              <style>
                              table, th, td {
                                  border: 1px solid black !important;
                                  border-collapse: collapse !important;
                              }
                              th, td {
                                  padding: 5px !important;
                                  text-align: center !important;
                                  font-size: 12px !important;
                              }
                              .color-vl {
                                background-color: #faff16 !important;
                                border-bottom: 4px solid #eee !important;
                              }
                              .subheading {
                                background-color: #FFF2CC !important;
                              }
                              .color-other { 
                                background-color : #fdf3cb !important;
                                border-bottom: 4px solid #eee !important;
                              }
                              .comp_off {
                                background-color: #D9D2E9 !important;
                              }
                              .sl {
                                background-color: #FFF2CC !important;
                              }
                              .vl {
                                background-color: #D9D9D9 !important;
                              }
                              .net {
                                background-color: #D5A6BD !important;
                              }
                              .total {
                                background-color: #EAD1CE !important;
                              }
                              .last-child + tr {
                                display: none;
                              }
                              </style>
                        </head>
                        <body id="ak" bgcolor="#e4e4e4" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="-webkit-font-smoothing: antialiased;width:100% !important;background:#e4e4e4;-webkit-text-size-adjust:none; padding-bottom:0; padding-left:0; padding-right:0; padding-top:0; font-family:sans-serif;">
                        <h1 text-align="center" font-size="20px" font-family="sans-serif">'.$subjectLine.'</h1>';
            $outPutStr .= '<table align="center" width="800px" cellpadding="0" cellspacing="0" border="0" bgcolor="#e4e4e4">';
            $outPutStr .= '<tr>
                              <th></th>
                              <th colspan="2">'.$headings[0].'</th>
                              <th colspan="2">'.$headings[1].'</th>
                              <th colspan="2">'.$headings[2].'</th>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th></th>
                            </tr>';
            $outPutStr .= '<tr>
                              <th></th>
                              <th class="sl">'.$subheadings[1].'</th>
                              <th class="vl">'.$subheadings[2].'</th>
                              <th class="sl">'.$subheadings[3].'</th>
                              <th class="vl">'.$subheadings[4].'</th>
                              <th class="sl">'.$subheadings[5].'</th>
                              <th class="vl">'.$subheadings[6].'</th>
                              <th class="comp_off">'.$subheadings[7].'</th>
                              <th class="comp_off">'.$subheadings[8].'</th>
                              <th class="comp_off">'.$subheadings[9].'</th>
                              <th class="total">'.$subheadings[10].'</th>
                            </tr>';
            $outPutStr .= ' <tr class="last-child" id="last-child">
                            <td>'.$e_name.'</td>
                            <td>'.$total_eligible_sl.'</td>
                            <td>'.$total_eligible_vl.'</td>
                            <td>'.$total_leav_tak_sl.'</td>
                            <td>'.$total_leav_tak_vl.'</td>
                            <td>'.$remain_sl.'</td>
                            <td>'.$remain_vl.'</td>
                            <td>'.$earned_comp.'</td>
                            <td>'.$taken_comp.'</td>
                            <td>'.$remain_comp.'</td>
                            <td>'.$total_penalty.'</td>
                          </tr>';
            $outPutStr .= '<table>';
            $outPutStr .= '</body>';
            $outPutStr .= '<html>';
            // var_dump($outPutStr);
            $to = trim($e_email);
            $subject =  $subjectLine;
            $headers[] = 'MIME-Version: 1.0';
            $headers[] = 'Content-type: text/html; charset=iso-8859-1';
            $headers[] = 'From: <'.$sender.'>';
            if(mail($to, $subject, $outPutStr, implode("\r\n", $headers))) {
              echo 'Mail has been send to '.$e_email.'</br>';
            }
            else {
              echo 'Mail sending fail to '.$e_email.'</br>';
            }
          }
          $k++;
        }
      }
    }
  }
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
  </head>
  <body>
    <h1>Leave Status Mailer!</h1>
    <form action="" name="leaveStatus"  method="post" enctype="multipart/form-data">
        <label for="fileToUpload">Please upload CSV Leave Status file: </label>
        <input type="file" accept=".csv" name="uploadedFile" id="fileToUpload" />
        <?php 
        if($isFormSubmitted AND !$isFileSelected) { 
            echo '<br/><span style="color:red">Please select file</span>';
        } else if($isFormSubmitted AND !$isValidFile) {
            echo '<br/><span style="color:red">Please use csv file</span>';
        } ?>
        <br /><input type="submit" name="doSendMail" value="Send Mail"/>
    </form>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  </body>
</html>
